---
title: Soil
date: 2022-12-06
---

[A Japanese inventor already has a patent on a soil acidity judging stick](https://patents.google.com/patent/JP2003279560A/en)

The last time i thought about this it was more of an auger with smarts. The EarthPlug. Expensive toy to leave lying around in the ground for continous soil quality monitoring though. It's all pretty abstract, I know that soil biogeochemists are passionate, but not why. 

I chatted with GPT about it (it's the flavour of the times):

Good morning! I'd like to learn more about soil quality monitoring

Of course! Soil quality refers to the overall health and fertility of the soil. It is a measure of how well the soil is able to support plant growth and maintain healthy ecosystems. There are many different factors that can impact soil quality, including soil pH, nutrient levels, and the presence of contaminants.

If you wanted a very low-tech way to measure either soil nutrients or soil contaminants, how would you go about it?

There are actually a few simple ways to test soil quality without the use of specialized equipment. One way to test for nutrient levels is to do a "tug test" by pulling on a handful of grass or other plants growing in the soil. If the plants pull out easily, it may indicate that the soil is lacking in nutrients. Another way to test for nutrients is to do a jar test. To do this, fill a jar with water and add a sample of soil to the water. Shake the jar well and then let it sit for a few hours. The layer of sediment that settles at the bottom of the jar can indicate the nutrient levels in the soil.

Interesting, how do you interpret the sediment in the bottom of the jar? And is it better to dig up a bit of soil or could you do it in the ground?

To interpret the sediment in the jar test, you would need to look at the color and thickness of the sediment layer. A thick layer of dark-colored sediment at the bottom of the jar can indicate that the soil is high in organic matter and nutrients. A thin, light-colored sediment layer can indicate that the soil is low in nutrients.

It is usually better to dig up a small sample of soil from the ground to do the jar test, rather than trying to do the test directly in the ground. This is because it is easier to get a consistent and representative sample of the soil when you dig it up. Plus, doing the test in a jar allows you to shake the soil and water mixture more easily, which can help to release any nutrients that may be bound up in the soil.

We went on to try to design a multi-wavelength spectrophotometer in a jar, but the suggestions went from over-engineered to crude. There must be something in between.

