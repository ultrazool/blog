---
title: "Jo Walsh's website"
description: "A personal website consisting of occasional blog posts and business pitches"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

