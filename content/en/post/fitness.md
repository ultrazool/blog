---
title: "Fitness"
date: 2023-01-02T12:00:00-05:00
---

It's been a while brewing, the inclination to learn how to teach group fitness. A random fellow student put it into my mind while we were waiting for a cancelled Zumba class. You could take up training, she said; H does it part-time, with a desk job on the side.

I talked to H and got her story - worked in finance, turned to fitness teaching after the 2008 crash, missed the disposable income enough to go back, but keep up a training practise for five hours a week, brings in more than enough to offset the cost while having fun doing it. 

It's a sort of flat pyramid scheme. There's a certifications racket. Qualifications bodies offer certificates that gyms need; instructor trainers sign up to re-sell the qualifications. That gets you the basics, then you need to attach to a brand to bring in an audience. Zumba, Les Mills, Pound; you become a monthly franchisee which gets you choreography and critically, licenced music.

It's the route to take - the one [Devi Sridhar took in qualifying as a personal trainer](https://www.theguardian.com/commentisfree/2023/jan/01/public-health-personal-trainer-pandemic) with the more involved, and expensive Level 3, above the Level 2 the municipal gym chain needs from instructors.

The last piece for me was going to classes by a local outfit just starting out, building a fitness business for themselves. They've got a big, sharp hook - [musical theatre meets high-intensity interval training](https://musicaltheatrefitness.com). At an early class I thought "the music doesn't work"; at another one, people were wearing fan club t-shirts. They have merch, and a roadmap to work the system, and require Level 2 to take their instructor training and become a franchisee.

Reader, i signed up for an online-delivery version of the certification. It's all recorded video conferences, online invigilated multi choice exams and send in a video of yourself delivering a 45 minute fitness class, hitting the grading points. The trustpilot reviews are great. The teacher looks like a Love Island contestant deciding how to move on in life, but her star student is a young Black woman doing classes in a south London community centre.

How much Online has opened all this up. The coursework I was expecting to focus on anatomy, on workarounds for injury, and technique - but a lot of what I've skim-viewed is about business-building, KPIs and SMART objectives, SWOT analyses, and their application to collective self-development, which is surprising and wonderful.

How very Gendered this all is. Most of the classes i go to have exclusively women students - some of the body conditioning ones have one or two men in an audience of 20, all the dance ones are women-only. But the movements are generic, for everyone. Is this stereotype threat? Is the dance fitness atmosphere as alienating for a lot of men as a free weights zone is for most women? How the hell are we conditioning ourselves physically to not belong?

It's not competitive, there's no ranking. The martial arts i practised for awhile, competition and ranking were the backbone of their support model. Fees for grading, fees for tournament entry, for seminar attendance towards more qualification. Even in a specifically non-profit art where no-one is making a living, only covering the costs of the community, so focused on status ranking. No blazers judging you. I don't miss it.

And what's a shy, enby, middle-aged nerd doing inserting themselves in all this? Taking one step at a time. Imagining themselves stepping into a performance persona, getting a crowd of sedentary hackers off their feet with a dance template. It could stay a fantasy or i could step onto the multi level marketing merry-go-round of mass fitness and find out whether it's possible to stay on it.

Because i believe it's more. Than we senselessly under-use ourselves as bodies. That in a post-work, post-wealth, post-romance world of humans looking around going "is this all there is", group fitness is a massive part of what there still is. Because it's a lot of fun doing it. And it would be very possible to end up kicking oneself very hard after over-analysing and partially monetising a source of joy. 
