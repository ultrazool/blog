---
title: "Proposition"
description: "Jo Walsh - 120 business plans"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

There are times when I have 120 business plans and can't give them away hard enough. 

The ambition here is to be on a small retainer / commission to be involved in a bootstrap phase, refine prototypes and facilitate workshops, and move along.


