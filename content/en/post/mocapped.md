---
title: "Don't Wannabe Mocapped"
date: 2022-12-23T12:00:00-05:00
---

Escaped a viewing of Avatar 2 due to a conveniently timed dose of flu, so my mum took my son to see it and came back primed with "mocap, how does that work then" questions which I was superficially able to answer thanks to a previous skim read of the wikipedia page after viewing some worrying uncritical machine learning mocap artworks, back in the summer. I wrote a pop lyric about it, back then - "Don't Mocap Me Now".

What worries me now is what worried me then. You assume actors have mocap derived work clauses in their contracts which they'd already have to be very powerful and influential to resist. They're already physically frozen with boto and variants, with prescriptive training and diet regimes. Now they're caputured, routinely, at a body and face surface level which makes their performance in any setting reproducible. 

When highly raised and trained performers are captured and reproducible in such complete detail, who would invest in the risk of supporting a new one? Prequels and sequels, bankable stars, perhaps we as a society have all the performers we need, the whole range is there, captured? Why feed and train a warehouse of putative pop idols when the ones with proven financial acclaim can go on forever?

It may come as a surprise to people who didn't spend the first 20 years of their adult life sedentary, but in early middle age I found a lot of novelty and satisfaction in training my body to do new things. Took up a couple of different Japanese martial arts, spent five years getting to cusp-of-senior level and burned out both times for fundamentally the same reason. 

The practise is trying to fit another body to yours. You're there, as many hours a week as you can spare, micro adjusting the angle and weight of your foot or your sword until you fit the pattern completely, until you channel the body language of someone who died decades or centuries ago, and if you don't like it, get the hell out of the transmission.

Maybe there's a martial art out there for me somewhere, maybe I'll make one. I took up fitness classes instead (that's another story). The whole time, you're fitting a different template to yourself. You find out where it bends, where it stretches, where it works. You feel a bit different each day and you fall for the process. You don't become perfectly tuned, but keep tuning yourself differently to hear how you sound.

I don't want to be mocapped. Not now, not in the past, not intentionally, not incidentally. Imagine there's no gait, just a template you can shrug on and off. Imagine it's already too late, that you already spent a couple of hours dancing behind a samba band doing a circuit of every bank headquarters and central government office in town, and the implicit picture of your finest tai chi / zumba / reggae fusion art is sitting in someone else's archives.

Imagine that ten thousand fold - how it must be for every film and stage performer in the world of VFX - how few gestures of an arm, shakes of a head and hip, it takes to capture who and where they are. Transpose them young, transpose them old, fuse them with someone better. Offer force feedback when they push against the template. Imagine how few people decide what shape the template is and who gets to inhabit it.

"You're over-analysing", my normie friend tells me when I share misgivings about putting myself forward for a speech synthesis gig that spoke to me - the research one would get to learn from, building emotionally plausible and persuasive synth voices. "Synthetic human behaviour is a bright line", I try to remind myself, as it gestures from around the corner. 

Here are the pop lyrics, anyway. You can imagine a performance of them while being mocapped doing it, and the projection behind you becomes increasingly outlandish. But for me the best performance is unplanned

## Don't Mocap Me (Now)

If you mocap me now
I'll never dance for you,
I'll always dance for you,
You'll never know it's true,
Don't mocap me now

Don't want to live a half-life in my own shadow,
Take everything you can see I've got from me now
You won't see what could have been,
Only an overlay
Won't see what should have been,
Only an underlay

I'd tell you i only dance when i want to but it's not true
I can't help it
Conduct the music or react with it, it's the same thing
The feeling
Of where it's going next
The raw memory you move
The loss landscape of the groove

If you mocap me now
You'll see me stop dancing
If you don't mocap me now
You'll never see me stop dancing
Don't mocap me (now)

