---
title: "Invisible women"
date: 2022-11-22T12:00:00-05:00
---

Evening presented by a film curator collective. Thread is feminist rediscovery of women in archive but not in history. After the showing talk to a few folks i know from bike protests. We talk about overlaps between scenes - bike couriers and InfraSisters. Critical Mass and OpenStreetmap.

I kickstarted OpenStreetmap, i tell them. That's so cool, they say. There's no trace of credit. Gave a bigshot Founder all of it: data, spec, protocol, social network and communications platform, while he protested his PhD supervisors said it wouldn't fly. Because i didn't want to maintain it. How intrisincally my contributions got written out of history. But the nodes remember.

