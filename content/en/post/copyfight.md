---
title: "The Copyfight"
date: 2023-08-06T12:00:00-05:00
---

That's me in the picture. It's 2005, and i'm standing on a stepladder at Speaker's Corner in Hyde Park, delivering a clearly impassioned lecture about the harm of Crown Copyright and the importance of liberating geographic data from the Ordnance Survey. The speech is heard mostly by the other visitors to Cory Doctorow's [London Copyfighters Drunken Brunch and Talking Shop](https://flickr.com/photos/suw/5703187)

![Jo on Speakers Corner, 20 years ago](../images/5703187_f2024806c3_c.jpg)

I wonder whether I'd be able to look Past Jo in the eye, today.

I didn't plan to end up working in AI Applied Research for the Ordnance Survey. I fell into the role as a side-effect of <a href="https://all-geo.org/volcan01010/2023/03/why-i-went-on-strike-over-civil-servant-pay/#lack-of-progression-harms-retention">unfortunate retention policies</a> at the British Geological Survey, which it was a wrench to leave. I needed the pricey private dental work, OS were taking an influx of interesting people, and the offer was too good to refuse.

It's not the incremental betrayal of everything that 2002-6 era Jo stood forwhich has broken me. You learn to care about other things; open data activism had been in the past for years. I was fairly sure I'd end up hitting the wall with OS after a year and a half; but the chance to carve out a niche for a good team, committed long-timer colleagues waiting for a respected organisation to renew its purpose, a forward looking post-pandemic approach to flexibility; combined with that strong sense of relative privilege and security during a time that's so much harder for so many; a lot of factors kept me seated.

I let go of a commitment to the social value of open data. I let go of more than barely perceptible movement towards work in the open on research software. I let go of the expectation of self-managed, affordable infrastructure. I let go of the expectation of release of work, gazing at Horizon X even as our team of teams got restructurally submerged into a market-focused Commercial entity.

What's broken me now is the realisation that not only is platform capitalism going to eat itself, it's taking the public sector with it, and I am _actively working towards it_. Every professional step I take, out of either fascination or frustration, is feeding the beast of Big Tech in a very specific and granular way that will reinforce vendor lock-in not just on our infrastructure but on the entire conceptual substrate of civil society. I've been avoiding confronting it for awhile, but am sure none of this is new to others.

I'm struggling with it hard, veering between the prosaic and the absurd. If you want, you can get absorbed in thought leadership, and in theory about pandemic-induced platform post-capitalism. You can outline detailed, book-length manifestos about how to steal back the Internet revolution for real, this time. You can critique that manifesto with a higher level view about a planet in pain and the overlooked suffering of billions of terrestrial creatures. You can call back to the Unabomber Manifesto or contemplate How To Survive the Coming Technological Singularity, and nod and frown at the critiques of Silicon Valley ideological extremists written by people who were advocating fully internal superveillance by a friendly supersingleton half a decade ago.

The second best time to start is now. I'd like to take the next step, just the one step, in the right direction. I'd like to know where people whose work I trust, think that direction is.  

I don't know whether this is a big tech AI hegemony rant, or a post-capitalist subsumption of the state rant, or just a me rant. That's why I'm writing any of this down (after a long time struggling with the futility and naivety and exploitability of writing anything down). I'll try to unpack small parts of it into larger pieces, and that will be for my own satisfaction in having managed to offload any of it.
