---
title: Research Software Engineering and Innovation in Environmental Science
date: 2024-02-27
---

_I was asked to offer a short presentation on "The role of Research Software Engineers in driving innovation in environmental science". This is for self-advocacy purposes so the narrative has a promotional, but still critical tone. There are slides, may add those later_

The presentation covers:

* The key role of an RSE in computational research
* The unexpected insights and savings that an RSE provides
* How Open Science has drawn from software practise
* The key role environmental science plays in modern compute infrastructure
* How mindful innovation can grow a policy impact

I can't make assumptions about culture because everyone's culture is different. But I'm glad to be offering this presentation to a group who understand the benefits of building up an RSE group, and what makes their engagement distinctive from researchers who code, and IT specialists in a research organisation. I'll both talk broadly about challenges, and introduce some specific personal experiences of innovation projects in a research setting. 

Bringing dedicated software engineering specialists into any research organisation is an encouraging sign. "Almost all research is computational" and for experiments and analysis to be robust, to be easy to reproduce and to defend, for stakeholders in different groups both to be able to contribute and to make meaningful use of the results, the skillset and experience of a dedicated research software engineer is crucial.

Is the role of an RSE ancillary, supportive in nature, focused on optimising experimental designs, improving speed and reducing resource usage of data analysis, working in hand with researchers in a specialist field. Is there more that an RSE can bring, not only to support innovation but to drive it?

The opportunity to work with different groups and to bridge disciplines, especially where computational research is driving new environmental discovery. I was hearing yesterday about an emerging specialism of "agrogeophysics" - "a discipline looking for indices to understand the complex interplay between the soil, the plant(s) and the atmosphere" - where tools and methods from one domain can be repurposed for and throw light upon another.

Domains don't even have to be adjacent to be structurally related! A small example from a past project was use of a Prometheus/Alertmanager system - originally designed to monitor and manage servers and applications - to collect and send notifications about volcano activity for a group of volcanologists who ran an on-call service for press and public sector information about volcano events. For a different project they'd worked with a data scientist to parse and prepare semi-structured data from volcano monitoring stations. A tiny web service to expose their data to Prometheus to collect it as a time-series, and a handful of rules to send Teams messages or emails when a threshold had been reached or the rate of alerts became frequent. As a rai prototype it took less than a day to set up on existing infrastructure, and sat there quietly running for a year until it was folded into a larger innovation project text mining detailed reports from volcano observatories. There was hope to do sentiment analysis on the resulting searchable archive, and map the reported metrics against the excitement level of the language sed in the volcano reports. It was time-saving, novel, avoided custom work and only came from an overlapping perspective in monitoring the health of network infrastructure.

Computational research practise has taken a lot from open source software engineering in the form of open science. Version control, testing and continuous integration, ongoing and incremental peer review. The best of corporate software development practises - UX-driven design sprints, "Agile" ways of working with frequent stakeholder engagement - have also been embedded in idealised views of open science and reproducible research, like the Turing Way's foray into communications practises and project design.

RSE innovation potential here is not only to affect the outcomes of new science, but also to affect the process, improving the way research is communicated, reviewed, and kept constantly upated. A platform like Curvenote - a partly open source offering to renew scientific communication through interactive, executable publications, emerging from the open source geoscience community - has the potential to modernise and reform how 21st century science is done.

But all is not rosy, especially as massive big tech companies with infrastructure to sell have moved into earth sciences applications and established a firm foothold for commercial or reputational reasons - Google's Earth Engine for remote sensing analysis, or Microsoft's more recent venture into Planetary Computer, bringing on contributors from open source analysis libraries and tools, investing in catalogue standards which bring research groups and public sector environmental organisations onto their platforms - for now, at a reduced price, but at what cost both in terms of skills development and true reprocibility of results?

Use of these cloud platforms for massive scale analysis has an environmental cost which is probably yet to become fully apparent. In a recent Nature piece Kate Crawford argued for a full role for environmental scientists in large infrastructure projects, working on the co-design of data centres to mitigate and monitor potential harms.

The role of research software and infrastructure engineers is key here, translating between different computing and research specialists, and working collaboratively to create reusable tools that can apply across projects and institutions, and introducing good practise from industry research into publically funded research.

A small example here is, in my previous role as a GeoAI Applied Research team lead, introducing the concept of Model Cards, originating from the Ethical AI research group co-led by Timnit Gebru at Google. I worked together with a graduate student on a year's placement and with a platform engineer to create python tooling that would estimate the co2 equivalent carbon cost of training deep learning models on Azure infrastructure, spread the documentation practise to other groups both within Innovation and mainstream Engineering, and through engagement with an organisation-wide AI working group, embedded model card creation in organisational policy for model publication and re-use - including detail on training data and methods, links to model registries with recommendations for reuse and caveats about unintended use.

True innovation should drive policy and planning. In this momentous effort across many disciplines and with many stakeholders, Research Software Engineers serve as essential glue.
 

   

  


