---
title: "Open source for Local Business"
description: "Jo Walsh - 120 business plans"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

## The hook

Small to medium sized businesses may be paying a lot more than they need for corporate IT packages, because they aren't aware of free and open source alternatives or they lack confidence and support to get set up on them. 

They may wish for good ethical reasons to avoid Silicon Valley platforms, and use of cloud-base services with environmental impact.

## The pitch

* Work with a small group of SMEs to identify shared needs for essential business services that (initially) aren't information-critical (e.g. avoid personally identifying information and financial information)

* Analyse their core costs and try to identify an open source stack that would serve as a replacement, offering _an improved user experience, less cost, an a better ethical basis_

* Pin down the messaging that would make this appeal to similar organisations - is painlessness, cheapness or harmlessness the priority?

A reasonable example is Canva for graphic design - £500/year for a small business licence for a service that's painful and confusing to use, and full of relentless AI marketing. What would a just-enough replacement look like?

## Extensions

Deepgreen Energy recently received a funding bump to install their tiny, eco-friendly datacentres (use waste heat to heat swimming pools) and are looking for infrastructure partners. 

Find a hosting provider that's already making a business arrangement with Deepgreen, and rent servers from them.

Find a design consultancy that's already doing a lot of work with local business in a sector of interest, and partner with them.

