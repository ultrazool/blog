---
title: "Developer Automation"
date: 2023-01-30T18:00:00-05:00
---

The end of another working day, eyeballs sore from seven hours, twenty-five minutes staring at a screen, thinking to myself, "why am i still doing this?"

In the day job I still do some software development, in amongst the people-wrangling. Software development, not writing code. Too much of a day spent lining up dependencies between conflicting versions of software packages; trying to pin down an obscure bug in mainstream code in a set of slightly different environments; finding workarounds for poor behaviours in a combination of Microsoft-maintained services. The grindy parts. 

Why still the same, after decades? During which the tech industry has seen so much illusion of progress, made so many promises. Now Github CoPilot and ChatGPT write code for us. But they don't do the grindy parts; the unfulfilling rough stuff, poking at consoles poring through stack traces. We automate the fun and fascinating parts, but not the gruntwork.

For obvious security reasons? Are there privileged worlds at scale that we're not a part of? Am I participating in ossified, outdated development practises, and if I worked in some mature and connected environment that exists for others, there wouldn't be these hundred tiny pain points? Are we all meant to hand off development and deployment to Google or Microsoft. In making and maintaining our own tools, are we cavepeople? 

In the rare moments of flow there are interfaces missing under my fingers - they have a felt shape, a data shape. We're teaching machines to do the same thing, in the same way. If we could automate away the grindy parts, would we not just bake in the substrate? Commit ourselves to another 20 years?

I'm tired of knowledge work. Dabbling in choreography has been wonderful because it's full-body thinking. What's left to me as a developer without big tech resources, it's hardly thinking at all.
