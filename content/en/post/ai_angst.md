---
title: "AI Angst"
date: 2022-11-27T12:00:00-05:00
---

_From a thread-of-threads written on a train journey, while processing the AI/ML track at the Research Software Engineering conference, about the exploding potential of generative models_  

Mind reeled a bit reading into and probing diffusers - the implications of this + CLIP variants that don't only consume image+text pairs but any set of dataset collections that correspond to one another

"Take any 2 (or N?) observations of the same thing at the same time, reduce them to long lines of numbers, boil the values away with noise, then keep trying to unboil them until your map of numbers shows you, from any starting point, where you are probably going".

I'll always remember @saul's line about "artists are the shock troops of gentrification" but what concept space is "AI art" gentrifying in this setting? The compute infrastructure energy/cost fiefdom is temporary. Systemic data secrecy, that's still the most core problem.

We could be dealing with _a lot, lot_ worse than artificial images in 10 years. I'd like to understand how what we have now could help avoid the worst of harm, and how to act on that. 

Social models, cultural models, datasets. A civic survey forms the next wave of gentrification. They call it a bidiectional leporello, but we are not fooled. The Gentrification of Species, perhaps.

We mine the archives, synthesise in the gaps. Nowadays we have mocap, very granular. A constant scan recording of a human face, and voice and stance. For actors it's tied into their contracts. Wrote pop lyrics about this once, "Don't Mocap Me Now".

Those time-travelling Mormons have a lot to answer for. This stuff has come along all at once. This incredible self-refining synthesis capacity at the same time as precipitious social and environmental collapse. You can see it going one of two ways - either fully automated luxury environmentalism - or the population of up-to-now as feedstock for billionaire fantasises of information capsule galactic domination.

We're aimed towards a cartoon version of the latter, with some likelihood that bias, participation and consent issues ignored by the bigcos won't get inflated into a cartoonishly horrific form. We become training data for another world that becomes training data for our own. Ubik.

_"I generate a colon in Blender"._

Let us for the moment confine ourselves to a healthcare setting. A service like Minder, it replaces daily-visit maintenance care packages. Monitor when someone visits the bathroom, when they use their appliances. All becomes more ad-hoc and occasional.

Care workers, already casualised and exploited, they go remote-first, monitors in a distributed care home call centre. But they're responding to modelling of events, their own behaviour is replaceable, synthetic care is indistinguishable.

You'd keep the human carers on their toes, rolling it out at first, maintain a constant training by mixing in synthetic patients. You'd find a tolerance threshold for missing adverse events, no matter where they came from. You could brush a lot of stuff under that threshold.

Now we remember such a system isn't designed only for folks with advanced symptoms of dementia, but also for those with a predicted likelihood. For folks suffering from "independence issues" due to whatever may currently pass for cognitive impairment.

Those who have "independence issues" thrust upon them find this is society's choice, whether this happens with intention or via a series of poorly defined objectives. Making it through the door becomes increasingly conditional.

In any healthcare-security setting. You're worth preserving while you're worth monitoring. And if not, what then?
