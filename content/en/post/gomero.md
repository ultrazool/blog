---
title: "La Gomera"
date: 2022-11-20T12:00:00-05:00
---

The fantasy about what it would be like to persuade someone to, however briefly, flee the geospatial tech industry and stay in a beach hut on one of the Canary Islands. Got as far as to look at a map of the Canary Islands, pick a relatively unknown but not insignificant one.

They have a whistling language, there. [Silbo Gomero](https://en.wikipedia.org/wiki/Silbo_Gomero), its use and essence dictated by the local geology.

So tired of speech primacy, how exploitable it is, phallologocentrism. Kicking myself very hard for the third thought being, "how would you train a model to intepret and synthesise the whistle of Silbo Gomero"

It would be good in the Canary Islands. They would offer a digital nomad visa and you could pay taxes and not have to whistle unless you wanted

