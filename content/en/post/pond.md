---
title: "The story of POND"
date: 2022-12-03T12:00:00-05:00
---

POND was a lockdown situational art project that i enjoyed in all its dimensions.

Started it in March 2020, had recently got out of Covid-19 quarantine (self-imposed, there were no tests available), and the limits on movement were an hour outside every day, no more than five miles from your residence.

I lived (still do, as of writing) around the corner from the Scottish Widows building - a listed structure of nested hexagons, tinted glass, and a street-facing carp pond. Many a time I'd stopped there for five minutes - with my child in a buggy, with him leaning precariously over the sloped walls, on my own - to watch the carp swim.

Chalk art's another thing a child does. It gets you out the house, offers a fifteen minute focus. I remember chalking a pandemic-preventing icon in the estate's back garden. The ephemerality works, the mark made to wash away.

I don't remember why I went out to draw chalk carp. On the opposite side of the wall from the pond. Swimming through chalk plants. There was no orange chalk so they were part-pink, part-yellow. They spread along the wall of the Scottish Widows, swimming to get out, swimming to get in. I was briefly stopped by the police. They drww up, very politely in their car, to observe what I was doing. Satisfied that I was not chalking revolutionary slogans, they drove off. I forgot about it.

Scottish Widows had long been bought out by a bank. The offices sat lockdown empty for some time. Deemed unattractive and unviable for more commerce, formally closed after a year or so. As a parting gift to the neighbourhood, the owners fished out the carp and drained the pond.

I remember seeing a young family wading in the sludge, with two toddler-aged kids, digging up the bulbs of water-lilies. The acrid smell as the surface vegetation started to rot in the sun. But this is Edinburgh, where it almost always rains, and slowly the pond started to re-fill. The herons, with no carp to attract them, buggered off, but ducks took up residence. You could bring a bucket of water at a time, but the weather would refill the pond by itself.

Going back to chalk more carp, they took on a totemic form. The memory of former carp and a kind of prayer for them coming back. Hopefully they were also a piece of comedy, something to make the neighbours look twice and smile. 

We went to a zine festival. The kid and a pal, their mum and i. A mum friend but a good friend, she'd never seen that part of counterculture before. We talked about the zines we might want to make, afterwards (thought about fitness culture, when asked). The concept that clicked later was POND. Greyscaled phone snaps of the pond in different states. Of generations of chalk carp. I went back, drew more carp and perched on the bridge to the office structure, making pencil sketches of the security cameras.

Printed photographs of graffiti on the toad road around Arthur's Seat. "No Going Back", and then a few months later the first word scrubbed out, now "Going Back". That was POND. The kid read it and said "For carp to return, water must return" was the best bit. That was someone else's response - Douglas Carnall's, off of Twitter.

A few weeks later we got a letter. We apologise for the inconvenience. There would be a consultation. A high-end architectural firm had been brought in to re-vision the Scottish Widows as a residential development, with a pruned-back office structure, it would all be sensitively done and would we like to participate in a public consultation.

So that was POND 2. Except the kid saw the sketch for the cover and made cutting remarks about how unimaginative a sequel name "POND 2" was. Given a plot outline, they suggested "POND and Politics".

I pasted up the colour cover and back page; made an unsuccessful attempt to resuscitate a battery-powered, water-activated robotic goldfish dating to early childhood; and reserved all judgement. Booked a half day off, and presented myself at the public consultation with a half-open mind.

They'd done a great job. The development company, the consulting architects, they'd really tried to make this a project of love. The history of the site, the ecological concern, the opening of walkthrough spaces to local residents, the limiting car parking to underground. On paper they'd made it work as well as it could work.

It threw my expectations for POND 2, which in the abstract were more confrontational and interpersonal. Went home and greyscaled the photos from the exhibition, the view of the pond from the stand of posters, the drawing on the paper response to the consultation (there was a form, and a lockbox to put the form in). Pasted, decorated, printed and sewed up a zine, then went back at the end of the afternoon and deposited POND and its sequel in the lockbox as a form of consultation response.

That's the story of POND. The proof sheets are slowly melting into the narrowly controlled areas of decay in my flat. Part of me thinks, "scan them into a PDF, link them here", and part of me thinks "deposit them no-where but with my mum, who latches onto these things for don't-know-why"

As of writing, the pond fills up again (without its substrate to attrack ducks), the redevelopment deadlines were 2025-6, it must be six months since i chalked a carp. You feel they're never gone, and shouldn't have been there. There's clearly space for "POND and Perseverance" as the third volume in a trilogy. Why is it so hard to imagine what it should be? 
