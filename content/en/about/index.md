---
title: "About"
description: ""
featured_image: '/images/Victor_Hugo-Hunchback.jpg'
menu:
  main:
    weight: 1
---

Never been a keeper of archives. They're scattered all over, or intentionally destroyed. Never considered it to make much difference, but perhaps that was fatalism or fear. Here's an attempt to (re)construct some.
