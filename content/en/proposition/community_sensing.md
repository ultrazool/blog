---
title: "Community Sensing"
description: "Combining remote sensing analysis with community survey - one of 120 business plans"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---


## The hook

There is a wealth of data collected by monitoring satellites that can track environmental change. Are we losing tree cover? Is our soil drying up? How fast are we losing land?

Large corporations are using this data to sell services to central government. Local community groups may think it is outside their budget, but it is not!

AI / machine learning is used to analyse it. There's a significant environmental footprint involved in doing this.

## The pitch

* A workshop with a local ecology group combining field survey and satellite data analysis
* User research to discover what analysis is most useful and how the interface can be made more straightforward
* Outcomes are data and reports on an area of interest, improvements to software for other groups 
 
An existing collaboration between a coder and a researcher has created a service for tracking and analysing environmental change in a local area. 

The analysis runs on _low-cost, low-energy hardware_ which they can either support directly, or help a local group to set up.

There's a web-based interface to create a geographic area and a time range of interest and create some statistical reports showing, for example, soil moisture levels and vegetation cover over time.

## The model

The constituency works on grant funding. Either a funding agency pays a subscription, or a grant includes a set cost which covers running the application - the data and analysis from which will always be freely available.


