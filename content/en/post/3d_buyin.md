---
title: "3D buy-in?"
date: 2023-03-07T12:00:00-05:00
---

I was sick for a few days, unspecifiable viral fatigue, and the kid lent me a Nintendo 3DS with "Legend of Zelda, Ocarina of Time". It's a fifteen year old piece of kit, the hook being a slider that warps the main screen into a stereoscope. No filters or glasses required - the screen blurs, you relax and engage your eyes, and have a tiny window into a 3D world. As your eyes tune in and you get used to it, tweaking the slider upwards increases the depth of the world.

The kid warned me off it - migraine-inducing - but it's been fine, the main aftereffect has been pixel scan lines superimposed over whatever flat surface I happen to be looking at, for a couple of hours afterwards. It reminds me of a brief time spent playing "Descent" in a VR headset in the late 1990s. The game made a new kind of sense; the world had no up, and the immersion made it more fluid to navigate. But for a while afterwards, the world was pixelated. I suppose the optic nerve / brain circuit gets so used to filtering the artefacts out, that for a while it works overtime to add them back in.

Is the current generation of 3D tech like that? Spend long enough in an Oculus Quest, what kind of psychic artefacts do you come out with? It makes a difference, experiencing another world in 3D, to how you remember it. My memories of playing the much more recent, flat-screen Zelda game on the Switch are very different in structure - a series of stills pasted awkwardly around the surfaces of a shape, not the shapes themselves. A pastiche of maps, rather than a model.

So many reasons to feel hesitant about metaverses. Not just the technical issues of scaling and the political one of near to full body motion tracking. The deeper ones about why we'd drive for audio-visual synthetic worlds to put humans in when audio-visual synthesis seems so near completely automatable. The disregard for what makes up the lived experience of humans - a cadre of technologists pursue this out of fascination, on a planet where billions go cold and hungry. The uncertainty of the end-point - these spaces designed to live so vividly in our minds, what are they for? The way our memories need things that are hard to fake - interesting smells, the potential joy of inhabiting a body. Do we get conditioned to want these things less, to forget what they are?




